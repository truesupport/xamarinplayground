using BLCore;
using MvvmCross;
using MvvmCross.IoC;
using MvvmCross.ViewModels;
using MyXamarinApp.Core.ViewModels.Main;

namespace MyXamarinApp.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            ConfigureDependencies();
            RegisterAppStart<MainViewModel>();
        }

        private void ConfigureDependencies()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IResortInfoProvider, HardcodedWithDelay>();
        }
    }
}
