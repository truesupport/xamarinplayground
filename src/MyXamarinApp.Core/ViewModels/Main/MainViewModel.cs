using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BLCore;
using MvvmCross.ViewModels;

namespace MyXamarinApp.Core.ViewModels.Main
{
    public class MainViewModel : BaseViewModel
    {
        private readonly IResortInfoProvider _resortInfoProvider;
        private string _slopesAmount = "123";
        public string SlopesAmount
        {
            get => _slopesAmount;
            private set => SetProperty(ref _slopesAmount, value);
        }

        private MvxNotifyTask _myTaskNotifier;
        public MvxNotifyTask MyTaskNotifier
        {
            get => _myTaskNotifier;
            private set => SetProperty(ref _myTaskNotifier, value);
        }

        public override async Task Initialize()
        {
            MyTaskNotifier = MvxNotifyTask.Create(LoadSlopesCountAsync, ex =>
            {
                SlopesAmount = ex.Message;
            });
        }

        public async Task LoadSlopesCountAsync()
        {
            var resortInfo = await _resortInfoProvider.GetResortInfo();
            SlopesAmount = resortInfo.Slopes.Count.ToString();
        }

        public MainViewModel(IResortInfoProvider resortInfoProvider)
        {
            _resortInfoProvider = resortInfoProvider;
        }
    }
}
