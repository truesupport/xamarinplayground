﻿using System;
using System.Threading.Tasks;
using Models.General;

namespace BLCore
{
    public class HardcodedWithDelay : HardcodedJsonStringResortInfoProvider, IResortInfoProvider
    {
        private static TimeSpan _delayTime = TimeSpan.FromSeconds(5);
        public override async Task<ResortInfo> GetResortInfo()
        {
            await Task.Delay(_delayTime);
            return await base.GetResortInfo();
        }
    }
}
