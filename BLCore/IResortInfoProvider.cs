using System;
using System.Threading.Tasks;
using Models.General;

namespace BLCore
{
    public interface IResortInfoProvider
    {
        Task<ResortInfo> GetResortInfo();
    }
}
