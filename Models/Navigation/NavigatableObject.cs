using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Navigation
{
    public abstract class NavigatableObject
    {
        public string Name { get; set; }
        public List<NavigatableObject> CanConnectTo { get; set; }
    }

    public enum NavigatableKind
    {
        Slope,
        Lift
    };

    public class NavigationRelation
    {
        public string From { get; set; }
        public string To { get; set; }
    }


}
