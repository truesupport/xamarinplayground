using System.Collections.Generic;
using System.Text;

namespace Models.General
{
    public class ResortInfo
    {
        public IList<Slope> Slopes { get; set; }
        public IList<Lift> Lifts { get; set; }
    }
}
