﻿namespace Models.General
{
    public enum SlopeStatus
    {
        Open,
        Close,
        Waiting
    }
}
