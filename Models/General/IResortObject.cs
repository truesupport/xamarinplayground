using System;
using System.Collections.Generic;

namespace Models.General
{
    public interface IResortObject
    {
        string Name { get; }
        int Distance { get; }
        int TopHeight { get; }
        int BottomHeight { get; }
        DateTime StartDate { get; set; }
        DateTime StopDate { get; set; }
        bool CanBeUsedInNavigation { get; }
        ResortObjectType Type { get; }
        IList<string> Neighbours { get; }
    }
}
