using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using GrabLiftsAndSlopesDataFromBukovelCom.Models.Networking;
using Models.General;
using Models.Navigation;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RestSharp;
using static GrabLiftsAndSlopesDataFromBukovelCom.Models.Networking.GetTrailsAndLifts;
using Lift = Models.General.Lift;

namespace GrabLiftsAndSlopesDataFromBukovelCom
{
    public class X
    {
        public List<IResortObject> Hello { get; set; } = new List<IResortObject>();
    }

    public class AB
    {
        public string From { get; set; }
        public string To { get; set; }
    }
    class Program
    {
        static async Task Main(string[] args)
        {
            //var data = await GetTrailsAndLiftsAsync();
            //await SaveToFileAsync("response.json", data);

            var ri = JsonConvert.DeserializeObject<ResortInfo>(File.ReadAllText(@"C:\Users\RS\Downloads\response_converted.json"));

            var x = new X();
            foreach (var item in ri.Lifts)
            {
                var ll = DeepClone(item);
                ll.Name = "L" + ll.Name;

                item.Name = "H" + item.Name;

                x.Hello.Add(ll);
                x.Hello.Add(item);
            }

            foreach(var zz in ri.Slopes)
            {
                x.Hello.Add(zz);
            }

            Regex r = new Regex("\"(.*?)\"");

            List<AB> h = new List<AB>();
            var rat = File.ReadAllLines(@"C:\Users\RS\Downloads\r.txt");
            foreach(var l in rat)
            {
                if (l.Trim().Length> 0)
                {
                    var mm =  r.Matches(l);
                    if (mm.Count == 2)
                    {
                        var Fromro = x.Hello.FirstOrDefault(x => x.Name == mm[0].Groups[1].Value);
                        var toRo = x.Hello.FirstOrDefault(x => x.Name == mm[1].Groups[1].Value);

                        if (Fromro != null && toRo != null)
                        {
                            Fromro.Neighbours.Add(toRo.Name);
                        }
                    }
                }
            }

            var s = JsonConvert.SerializeObject(x.Hello);

            //foreach(var f in x.Hello)
            //{
            //    var f.Neighbours =  f.Neighbours.Distinct();
            //}
            
        }


        public static async Task<Response> GetTrailsAndLiftsAsync()
        {
            const string url = "https://bukovel.com/api/v1/ski/map/trails/lifts";
            RestClient c = new RestClient();
            System.Console.WriteLine("Getting info from " + url);
            var response = await c.ExecuteAsync<Models.Networking.GetTrailsAndLifts.Response>(new RestRequest(url));

            System.Console.WriteLine("Response status code: " + response.StatusCode);
            System.Console.WriteLine($"Received info about {response.Data.LiftsData.Count} lifts and {response.Data.TracksData.Count} tracks");

            return response.Data;
        }

        public static T DeepClone<T>(T obj)
        {
            var s = JsonConvert.SerializeObject(obj);
            var r = JsonConvert.DeserializeObject<T>(s);
            return r;
        }



        public static async Task SaveToFileAsync(string fileName, Models.Networking.GetTrailsAndLifts.Response data)
        {
            var content = JsonConvert.SerializeObject(data);
            Console.WriteLine("Saving raw info to " + Path.GetFullPath(fileName));
            await File.WriteAllTextAsync(fileName, content);

            var rr = new ResortInfo
            {
                Lifts = data.LiftsData.Select(x => ToMLift(x.Value)).ToList(),
                Slopes = data.TracksData.Select(x => ToMSlope(x.Value)).ToList()
            };

            var convertedFileName = Path.GetFileNameWithoutExtension(fileName) + "_converted.json";
            Console.WriteLine("Saving converted info to " + Path.GetFullPath(convertedFileName));
            File.WriteAllText(convertedFileName, JsonConvert.SerializeObject(rr, new StringEnumConverter()));
        }

        public static Slope ToMSlope(Track track)
        {

            if (string.IsNullOrEmpty(track.Info.BottomHeight))
            {
                track.Info.BottomHeight = "0";
            }

            if (string.IsNullOrEmpty(track.Info.Distance))
            {
                track.Info.Distance = "0";
            }

            

            if (string.IsNullOrEmpty(track.Info.TopHeight))
            {
                track.Info.TopHeight = "0";
            }


            var mSlope = new Slope
            {
                BottomHeight = int.Parse(track.Info.BottomHeight),
                Distance = int.Parse(track.Info.Distance),
                LiftId = track.Info.Lift.Name,
                Name = track.Info.Name,
                Notes = track.Info.Notes.Select(x => x.Text).ToList(),
                StartDate = track.StartDate,
                StopDate = track.StopDate,
                TopHeight = int.Parse(track.Info.TopHeight),
            };

            mSlope.Difficulity = (SlopeDifficulty)Enum.Parse(typeof(SlopeDifficulty), track.Difficulty.ToString(), true);
            mSlope.Status = (SlopeStatus)Enum.Parse(typeof(SlopeStatus), track.Status.ToString(), true);


            return mSlope;

        }

        public static Lift ToMLift(GetTrailsAndLifts.Lift lift)
        {
            if (string.IsNullOrEmpty(lift.Info.BottomHeight))
            {
                lift.Info.BottomHeight = "0";
            }

            if (string.IsNullOrEmpty(lift.Info.Distance))
            {
                lift.Info.Distance = "0";
            }

            if (string.IsNullOrEmpty(lift.Info.Traffic))
            {
                lift.Info.Traffic = "0";
            }

            if (string.IsNullOrEmpty(lift.Info.TopHeight))
            {
                lift.Info.TopHeight = "0";
            }
            var mLift = new Lift
            {
                BottomHeight = int.Parse(lift.Info.BottomHeight),
                Distance = int.Parse(lift.Info.Distance),
                IsOpen = lift.IsOpen,
                Name = lift.Info.Name,
                StartDate = lift.StartDate,
                StopDate = lift.StopDate,
                TopHeight = int.Parse(lift.Info.TopHeight),
                Traffic = int.Parse(lift.Info.Traffic)
            };

            var type = lift.Info.Type;

            if (type.StartsWith("3"))
            {
                mLift.Type = LiftType.Seat3;
            }

            if (type.StartsWith("4"))
            {
                mLift.Type = LiftType.Seat4;
            }

            if (type.StartsWith("6"))
            {
                mLift.Type = LiftType.Seat6;
            }


            if (string.Equals(type, "BUGIL", System.StringComparison.InvariantCultureIgnoreCase))
            {
                mLift.Type = LiftType.Bugil;
            }

        return mLift;

       
        }
    }
}
