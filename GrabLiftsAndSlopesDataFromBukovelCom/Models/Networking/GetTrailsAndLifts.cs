using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace GrabLiftsAndSlopesDataFromBukovelCom.Models.Networking
{
    public class GetTrailsAndLifts
    {
        
        public class Response
        {
            [JsonProperty("tracksData")]
            public Dictionary<string, Track> TracksData { get; set; }

            [JsonProperty("liftsData")]
            public Dictionary<string, Lift> LiftsData { get; set; }
        }

        public class Track
        {
            [JsonProperty("status")]
            public string Status { get; set; }

            [JsonProperty("start_date")]
            public DateTime StartDate { get; set; }

            [JsonProperty("stop_date")]
            public DateTime StopDate { get; set; }

            [JsonProperty("difficulty")]
            public Dificulity Difficulty { get; set; }

            [JsonProperty("info")]
            public Info Info { get; set; }

            [JsonProperty("date_modified")]
            public DateTime DateModified { get; set; }
        }

        public enum Dificulity
        {
            Blue,
            Red,
            Black
        }

        public class Note
        {
            [JsonProperty("id")]
            public int Id { get; set; }

            [JsonProperty("text")]
            public string Text { get; set; }
        }


        public class Info
        {
            [JsonProperty("name")]
            public string Name { get; set; }

            [JsonProperty("distance")]
            public string Distance { get; set; }

            [JsonProperty("notes")]
            public List<Note> Notes { get; set; }

            [JsonProperty("top_height")]
            public string TopHeight { get; set; }

            [JsonProperty("lift")]
            public LiftInfo Lift { get; set; }

            [JsonProperty("bottom_height")]
            public string BottomHeight { get; set; }

            [JsonProperty("traffic")]
            public string Traffic { get; set; }

            [JsonProperty("type")]
            public string Type { get; set; }
        }

        public class LiftInfo
        {
            [JsonProperty("id")]
            public int Id { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }
        }

        public class Lift
        {
            [JsonProperty("isOpen")]
            public bool IsOpen { get; set; }

            [JsonProperty("start_date")]
            public DateTime StartDate { get; set; }

            [JsonProperty("stop_date")]
            public DateTime StopDate { get; set; }

            [JsonProperty("info")]
            public Info Info { get; set; }

            [JsonProperty("date_modified")]
            public DateTime DateModified { get; set; }
        }
    }
}
